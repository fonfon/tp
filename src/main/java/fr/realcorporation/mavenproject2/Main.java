/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.mavenproject2;

import fr.realcorporation.mavenproject2.POJO.Adress;
import fr.realcorporation.mavenproject2.POJO.Place;
import fr.realcorporation.mavenproject2.POJO.Person;
import fr.realcorporation.mavenproject2.POJO.Place.Type;
import fr.realcorporation.mavenproject2.POJO.Student;
import fr.realcorporation.mavenproject2.POJO.Teacher;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * exercice 0 exercice fini
 */
public class Main {

    private static Logger log = Logger.getLogger(Main.class.getName());
    private static Scanner scan = new Scanner(System.in);

    public static void main(final String[] TestArg) {
        System.out.println("Saisir un nombre: ");
        System.out.println("saisir -1 pour sortir");
        int nombre = scan.nextInt(); // permet de saisir une valeur
        while (nombre != -1) { // tant  que l'utilisateur ne rentre pas -1 il lui redemandera de rentre une valeur a chaque boucle
            TestArg[0] = "salut" + " " + facto(nombre);
            log.log(Level.INFO, TestArg[0]); //on attend que le resultat de TestArg[0] soit affiché dans la console
            System.out.println("Saisir un nombre: ");
            System.out.println("saisir -1 pour sortir");
            nombre = scan.nextInt();
        }

        /**
         * exercice 3 exercice fini
         */
        Adress adress1;
        adress1 = new Adress(39, "route de la haye", " pouet ", 41120, "Les Montils", "France", 0.1151154, 0.2514681631);
        log.log(Level.INFO, adress1.toString());

        /**
         * exercice 4 exercice fini
         */
        Place place1;
        place1 = new Place("jean-bedoin", adress1, Type.ecole, "petite école");
        log.log(Level.INFO, place1.toString());

        /**
         * exercice 5 exercice fini
         */
        Person person1;
        person1 = new Person("Edouard", "Jean", adress1);
        log.log(Level.INFO, person1.toString());
        
        /**
     * exercice 6 
     * exercice fini
     */
    Teacher teacher1;
    teacher1  = new Teacher("mathématique", "jarre", "jean-michel", adress1);
    log.log(Level.INFO, teacher1.toString());
    
    Student student1;
    student1 = new Student("seconde", "papy", "mouzo", adress1);
    log.log(Level.INFO, student1.toString());
    }

    
    
    /**
     * exercice 1 et 2 exercice fini
     */
    public static int facto(int nombre) {
        if (nombre > 0) {
            return nombre * facto(nombre - 1);
        } else {
            return 1;
        }
    }
}
