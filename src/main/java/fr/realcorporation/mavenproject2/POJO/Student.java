/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.mavenproject2.POJO;

/**
 *exercice 6
 */
public class Student extends Person {
    String classe;

    public Student(String classe, String name, String firstname, Adress adress) {
        super(name, firstname, adress);
        this.classe = classe;
    }
    
                @Override
    public String toString() {
        return "le nom de l'élève est : " + this.name + ", le prénom : " + this.firstname + " habite " +this.adress + " et est en " + classe;
    }
}
