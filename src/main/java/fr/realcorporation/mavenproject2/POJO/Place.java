/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.mavenproject2.POJO;

/**
 * exercice 4
 *
 */
public class Place {

    String name;
    Adress adress;
    Type type;
    String note;

    public Place(String name, Adress adress, Type type, String note) {
        this.name = name;
        this.adress = adress;
        this.type = type;
        this.note = note;
    }

    public enum Type{
        appartement,
        ecole,
        cinema,
        salle;
    }

    @Override
    public String toString() {
        return "le nom du lieu est " + this.name + " situé " + this.adress + " de " + this.type + " note : " + this.note;
    }

}
