/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.mavenproject2.POJO;

/**
 * exercice 3
 */
public class Adress {

    int numberStreet;
    String street1;
    String street2;
    int postalCode;
    String city;
    String country;
    double latitude;
    double longitude;

    public Adress(int numberStreet, String street1, String street2, int postalCode, String city, String country, double latitude, double longitude) {
        this.numberStreet = numberStreet;
        this.street1 = street1;
        this.street2 = street2;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Adress(int numberStreet, String street1, String street2, int postalCode, String city, String country) {
        this.numberStreet = numberStreet;
        this.street1 = street1;
        this.street2 = street2;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
    }

    @Override
    public String toString(){
        return "le numéro de rue est " + this.numberStreet + " dans la rue " + this.street1 + this.street2  + ", le code postal est " + this.postalCode + " à " + this.city + " en " + this.country + " de latitude " + this.latitude + " et de longitude " + this.longitude + ".";   
    }
    
}
