/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.mavenproject2.POJO;

/**
 *exercice 6
 */
public class Teacher extends Person {
    String subject;
    
    public Teacher(String subject, String name, String firstname, Adress adress) {
        super(name, firstname, adress);
        this.subject = subject;
    }
    
            @Override
    public String toString() {
        return "le nom du professeur est : " + this.name + ", le prénom : " + this.firstname + " habite " +this.adress + " et enseigne " + subject;
    }
    
    
}
