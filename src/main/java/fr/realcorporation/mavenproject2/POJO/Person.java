/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.mavenproject2.POJO;

/**
 *exercie 5
 * @author cfontaine
 */
public class Person {
    String name;
    String firstname;
    Adress adress;

    public Person(String name, String firstname, Adress adress) {
        this.name = name;
        this.firstname = firstname;
        this.adress = adress;
    }
    
        @Override
    public String toString() {
        return "le nom est : " + this.name + ", le prénom : " + this.firstname + " habite " +this.adress;
    }
    
    
}

